package app.akexorcist.bluetoothspp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;

public class MatrixActivity extends Activity {
    public static final String TAG = "MATRIX";

    public static final int NUM_MATIRX = 32;            // 4 by 8
    public static final int NUM_MATRIX_HORIZONTAL_X = 4;
    public static final int NUM_MATRIX_VERTICAL_Y = 8;
    public static final int NUM_CHANNEL = 12;
    public static final int NUM_RECEIVE_BYTE = 24;      // 12channel * 2byte = 24byte

    private TextView[][] mTextMatrix;
    private TextView[] mTextChannel;
    private TextView mTextStatus;

    private BluetoothSPP bt;
    private Menu menu;

    private int[] mChannel;
    private int[] mThresChannel;
    private int[][] mValueMatrix;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matrix_4x8);



        initMatrix();
        configThreshold();

        bt = new BluetoothSPP(this);

        if (!bt.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext()
                    , "Bluetooth is not available"
                    , Toast.LENGTH_SHORT).show();
            finish();
        }

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceDisconnected() {
                mTextStatus.setText("Status : Not connect");
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_connection, menu);
            }

            public void onDeviceConnectionFailed() {
                mTextStatus.setText("Status : Connection failed");
            }

            public void onDeviceConnected(String name, String address) {
                mTextStatus.setText("Status : Connected to " + name);
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_disconnection, menu);
            }
        });

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(final byte[] data, final String message) {
                if (message.length() != 24)
                    return;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //updateMatrix_4by8(data, message);

                        updateAllChannelMatrix_4by8(message);
                    }
                });

                byte b[] = message.getBytes();
                //Log.d(TAG, ""+message.length());

                /*
                for (int i = 0; i < message.length(); i++) {
                    Log.d(TAG, ""+b[i]+" "+i);
                }*/
                mTextStatus.setText(message);
            }
        });



    }

    private void updateAllChannelMatrix_4by8(String channelStringMsg) {
        decodeMessageForAllChannel(channelStringMsg);
        valueMatrix();
        coloringMatrix();
    }

    private void valueMatrix() {
        mValueMatrix = new int[NUM_MATRIX_HORIZONTAL_X][NUM_MATRIX_VERTICAL_Y];

        for (int i = 0; i < NUM_CHANNEL; i++) {
            if (mChannel[i] < mThresChannel[i]) {
                if (i < NUM_MATRIX_HORIZONTAL_X) {
                    for (int j = 0; j < NUM_MATRIX_VERTICAL_Y; j++) {
                        mValueMatrix[i][j] += 1;
                    }
                }
                else {
                    for (int j = 0; j < NUM_MATRIX_HORIZONTAL_X; j++) {
                        mValueMatrix[j][i-NUM_MATRIX_HORIZONTAL_X] += 1;
                    }
                }
            }
        }
    }

    private void decodeMessageForAllChannel(String allMsg) {
        mChannel = new int[NUM_CHANNEL];
        byte[] arrBytes = allMsg.getBytes(StandardCharsets.US_ASCII);
        int positiveNumber[] = new int[NUM_RECEIVE_BYTE];

        Log.d(TAG, ""+allMsg.length());

        for (int i = 0; i < NUM_RECEIVE_BYTE; i++) {
            if (arrBytes[i] < 0)
                positiveNumber[i] = 256 + ((int) arrBytes[i]);
            else
                positiveNumber[i] = ((int) arrBytes[i]);
        }

        for (int i = 0; i < NUM_CHANNEL; i++) {
            mChannel[i] = positiveNumber[i] + (positiveNumber[i*2+1] << 8);
            mTextChannel[i].setText(""+mChannel[i]);
        }
    }

//    private void updateMatrix_4by8(byte[] data, String msg) {
//        int idx = 99;
//        idx = decodeMessageForEachChannel(msg);
//
//        valueMatrix(idx);
//        coloringMatrix();
//    }

//    private void valueMatrix(final int idxOfChannel) {
//        mValueMatrix = new int[NUM_MATRIX_HORIZONTAL_X][NUM_MATRIX_VERTICAL_Y];
//
//        // init matrix value
//        if (idxOfChannel == 0 || idxOfChannel == 99) {
//            for (int i = 0; i < NUM_MATRIX_HORIZONTAL_X; i++) {
//                for (int j = 0; j < NUM_MATRIX_VERTICAL_Y; j++) {
//                    //mValueMatrix[i][j] = 0;
//                    return;
//                }
//            }
//        }
//        else
//        {
//            // plus (+)
//            if (mChannel[idxOfChannel] < mThresChannel[idxOfChannel]) {
//                if (idxOfChannel < NUM_MATRIX_HORIZONTAL_X) {           // X-AXIS channel(#0 ~ 3) flagging
//                    for (int i = 0; i < NUM_MATRIX_VERTICAL_Y; i++) {
//                        mValueMatrix[idxOfChannel][i] += 1;
//                    }
//                }
//                else {                                                  // Y-AXIS channel(#4 ~ 11)
//                    for (int i = 0; i < NUM_MATRIX_HORIZONTAL_X; i++) {
//                        mValueMatrix[i][idxOfChannel-NUM_MATRIX_HORIZONTAL_X] += 1;
//                    }
//                }
//            }
//            else {      // minus (-)
//                if (idxOfChannel < NUM_MATRIX_HORIZONTAL_X) {
//                    for (int i = 0; i < NUM_MATRIX_VERTICAL_Y; i++) {
//                        mValueMatrix[idxOfChannel][i] -= 1;
//                    }
//                }
//                else {
//                    for (int i = 0; i < NUM_MATRIX_HORIZONTAL_X; i++) {
//                        mValueMatrix[i][idxOfChannel-NUM_MATRIX_HORIZONTAL_X] -= 1;
//                    }
//                }
//            }
//        }
//    }

    private void configThreshold() {
        mThresChannel = new int[NUM_CHANNEL];

        mThresChannel[0] = 500;     mThresChannel[1] = 500;     mThresChannel[2]  = 500;     mThresChannel[3]  = 500;
        mThresChannel[4] = 500;     mThresChannel[5] = 500;     mThresChannel[6]  = 500;     mThresChannel[7]  = 500;
        mThresChannel[8] = 500;     mThresChannel[9] = 500;     mThresChannel[10] = 500;     mThresChannel[11] = 500;
    }

    private void coloringMatrix() {
        for (int i = 0; i < NUM_MATRIX_HORIZONTAL_X; i++) {
            for (int j = 0; j < NUM_MATRIX_VERTICAL_Y; j++) {
                mTextMatrix[i][j].setText(""+mValueMatrix[i][j]);

                if (mValueMatrix[i][j] == 2)
                    mTextMatrix[i][j].setBackgroundColor(Color.RED);
                else
                    mTextMatrix[i][j].setBackgroundColor(Color.WHITE);
            }
        }
    }

//    private int decodeMessageForEachChannel(String msg) {
//        mChannel = new int[NUM_CHANNEL];
//        int idx = 99;
//        String strMsg, strIdx, strCh;
//        strMsg = msg;
//
//        if (msg.charAt(0) == '$') {
//            strIdx = strMsg.substring(1,3);
//            idx = Integer.parseInt(strIdx);
//            strCh = msg.substring(4);
//            mChannel[idx] = Integer.parseInt(strCh);
//            //Log.d(TAG,""+idx+" "+mChannel[idx]);
//            mTextChannel[idx].setText(""+mChannel[idx]);
//        }
//        return idx;
//    }

    private void initMatrix() {
        mTextMatrix  = new TextView[NUM_MATRIX_HORIZONTAL_X][NUM_MATRIX_VERTICAL_Y];
        mTextChannel = new TextView[NUM_CHANNEL];


        for (int i = 0; i < NUM_MATIRX; i++) {
            int resID;
            resID = getResources().getIdentifier("app.akexorcist.bluetoothspp:id/textView_M" + (i+1), null, null);

            mTextMatrix[i/8][i%8] = (TextView) findViewById(resID);

            /*
            if (i < 8)
                mTextMatrix[0][i] = (TextView) findViewById(resID);
            else if (i < 16 && i >= 8)
                mTextMatrix[1][i-8] = (TextView) findViewById(resID);
            else if (i < 24 && i >= 16)
                mTextMatrix[2][i-16] = (TextView) findViewById(resID);
            else
                mTextMatrix[3][i-24] = (TextView) findViewById(resID);*/
            //Log.d(TAG,"ID"+mTextMatrix[i/8][i%8]);
        }

        for (int i = 0; i < NUM_CHANNEL; i++) {
            int resID;

            if (i < 9)
                resID = getResources().getIdentifier("app.akexorcist.bluetoothspp:id/textView_CH0" + (i + 1), null, null);
            else
                resID = getResources().getIdentifier("app.akexorcist.bluetoothspp:id/textView_CH" + (i + 1), null, null);

            mTextChannel[i] = (TextView) findViewById(resID);
        }

        mTextStatus  = (TextView) findViewById(R.id.matrix_textStatus);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_connection, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_android_connect) {
            bt.setDeviceTarget(BluetoothState.DEVICE_ANDROID);
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        } else if (id == R.id.menu_device_connect) {
            bt.setDeviceTarget(BluetoothState.DEVICE_OTHER);
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        } else if (id == R.id.menu_disconnect) {
            if (bt.getServiceState() == BluetoothState.STATE_CONNECTED)
                bt.disconnect();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }

    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if (!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
            } else {
                Toast.makeText(getApplicationContext()
                        , "Bluetooth was not enabled."
                        , Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
